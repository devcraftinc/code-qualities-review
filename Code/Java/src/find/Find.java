// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package find;


public class Find {
    public interface TypeOfCandy {
    }

    public interface TypeOfTopping {
    }

    public static class CategoryTypeOfTopping implements TypeOfTopping {
        private final String _Category;

        public CategoryTypeOfTopping(String category) {
            _Category = category;
        }

        public String category() {
            return _Category;
        }
    }

    public static class FlavorTypeOfTopping implements TypeOfTopping {
        public FlavorTypeOfTopping(String flavor) {
            _Flavor = flavor;
        }

        private String _Flavor;

        public String flavor() {
            return _Flavor;
        }
    }

    public static class CompositeTypeOfCandy implements TypeOfCandy {
        public CompositeTypeOfCandy(TypeOfCandy[] parts) {
            _Parts = parts;
        }

        private TypeOfCandy[] _Parts;

        public TypeOfCandy[] parts() {
            return _Parts;
        }
    }

    public static class HardTypeOfCandy implements TypeOfCandy {
    }

    public static class TaffyTypeOfCandy implements TypeOfCandy {
    }

    public static class FruitTypeOfCandy implements TypeOfCandy {
    }

    public static class ShelledTypeOfCandy implements TypeOfCandy {
    }

    public static class ChocolateTypeOfCandy implements TypeOfCandy {
    }

    public static class NougatTypeOfCandy implements TypeOfCandy {
    }

    public candy.Candy it(TypeOfCandy t) {
        String tableName = "e_candy";
        String where = "";

        TypeOfCandy[] types = ((CompositeTypeOfCandy) t).parts();
        String del = " WHERE ";
        for (TypeOfCandy type : types) {
            if (type instanceof HardTypeOfCandy) {
                where += del;
                where += "IsHard = 1";
                del = ", ";
            } else if (type instanceof TaffyTypeOfCandy) {
                where += del;
                where += "IsTaffy=1";
                del = ", ";
            } else if (type instanceof ShelledTypeOfCandy) {
                where = where + del + "IsShelled=1";
                del = ",";
            } else if (type instanceof ChocolateTypeOfCandy) {
                where = where + del;
                where += "Type='Chocolate'";
                del = " ,";
            } else {
                where += del;
                where += "Type='Fruit'";
                del = ", ";
            }
        }

        candy.Candy[] candies = RemoteServerDataWarehouse_v2.selectAndParseCandy(tableName + where);

        if (candies.length == 0)
            return null;

        if (candies.length > 2)
            throw new IllegalStateException();

        return candies[0];
    }

    public candy.Candy[] them(TypeOfCandy t) {
        String tableName = "e_candy";
        String where = "";

        TypeOfCandy[] types = ((CompositeTypeOfCandy) t).parts();
        String del = " WHERE ";
        for (TypeOfCandy type : types) {
            if (type instanceof HardTypeOfCandy) {
                where += del;
                where += "IsHard = 1";
                del = ", ";
            } else if (type instanceof TaffyTypeOfCandy) {
                where += del;
                where += "IsTaffy=1";
                del = ", ";
            } else if (type instanceof ShelledTypeOfCandy) {
                where = where + del + "IsShelled=1";
                del = ",";
            } else if (type instanceof ChocolateTypeOfCandy) {
                where = where + del;
                where += "Type='Chocolate'";
                del = " ,";
            } else {
                where += del;
                where += "Type='Fruit'";
                del = ", ";
            }
        }

        return RemoteServerDataWarehouse_v2.selectAndParseCandy(tableName + where);
    }

    public toppings.Topping it(int id, TypeOfTopping[] t) {
        String tableName = "e_toppings";
        String where = " WHERE cancompatid=" + id;

        String del = " ,";
        for (TypeOfTopping typeOfTopping : t) {
            if (typeOfTopping instanceof CategoryTypeOfTopping) {
                CategoryTypeOfTopping category = (CategoryTypeOfTopping) typeOfTopping;
                where += del;
                where += "Category='" + category.category() + "'";
                del = ",";
            }

            if (typeOfTopping instanceof FlavorTypeOfTopping) {
                FlavorTypeOfTopping flavor = (FlavorTypeOfTopping) typeOfTopping;
                where += del;
                where += "Flavor='" + flavor.flavor() + "'";
                del = ",";
            }
        }

        toppings.Topping[] candies = RemoteServerDataWarehouse_v2.selectAndParseTopping(tableName + where);

        if (candies.length == 0)
            return null;

        if (candies.length > 2)
            throw new IllegalStateException();

        return candies[0];
    }

    public toppings.Topping itPremium(int id, TypeOfTopping[] t) {
        String tableName = "e_premtops";
        String where = " WHERE cancompatid=" + id;

        String del = " ,";
        for (TypeOfTopping typeOfTopping : t) {
            if (typeOfTopping instanceof CategoryTypeOfTopping) {
                CategoryTypeOfTopping category = (CategoryTypeOfTopping) typeOfTopping;
                where += del;
                where += "PremiumCategory='" + category.category() + "'";
                del = ",";
            }

            if (typeOfTopping instanceof FlavorTypeOfTopping) {
                FlavorTypeOfTopping flavor = (FlavorTypeOfTopping) typeOfTopping;
                where += del;
                where += "PremiumFlavor='" + flavor.flavor() + "'";
                del = ",";
            }
        }

        toppings.Topping[] candies = RemoteServerDataWarehouse_v2.selectAndParseTopping(tableName + where);

        if (candies.length == 0)
            return null;

        if (candies.length > 2)
            throw new IllegalStateException();

        return candies[0];
    }
}


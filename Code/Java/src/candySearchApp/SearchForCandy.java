// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package candySearchApp;

import java.util.*;

public class SearchForCandy extends FruitMissingABytePage {
    private final Map<String, String> values = new HashMap<>();
    private String[] buttons = {"Search"};
    private Map<String, String> data;

    @Override
    protected Map<String, String> getProperties() {
        return values;
    }

    @Override
    protected String[] defineButtons() {
        return buttons;
    }

    @Override
    protected void defineFields(Map<String, String[]> options) {
        options.put("Is Hard", new String[]{"Yes", "No"});
        options.put("Type", new String[]{"Nougat", "Chocolate", "Fruit"});
        options.put("Is Taffy", new String[]{"Yes", "No"});
        options.put("Is Shelled", new String[]{"Yes", "No"});
    }

    @Override
    protected void setFields(Map<String, String> fieldValues) {
        data = fieldValues;
    }

    @Override
    protected void buttonPushed(String id) {
        if (id == "Search") {
            ArrayList<find.Find.TypeOfCandy> qualities = new ArrayList<>();
            if ("Yes".equals(data.get("Is Hard")))
                qualities.add(new find.Find.HardTypeOfCandy());

            if ("Yes".equals(data.get("Is Taffy")))
                qualities.add(new find.Find.TaffyTypeOfCandy());

            if (!"No".equals(data.get("Is Shelled")))
                qualities.add(new find.Find.ShelledTypeOfCandy());

            if ("Nougat".equals(data.get("Type")))
                qualities.add(new find.Find.NougatTypeOfCandy());
            else if ("Chocolate".equals(data.get("Type")))
                qualities.add(new find.Find.ChocolateTypeOfCandy());
            else
                qualities.add(new find.Find.FruitTypeOfCandy());

            try {
                candy.Candy candy = new find.Find().it(new find.Find.CompositeTypeOfCandy(qualities.toArray(new find.Find.TypeOfCandy[0])));
                if (candy != null) {
                    values.put("Name", candy.szf_Ttl());
                    values.put(": Stock", candy.i4f_Cnt() > 0 ? "Yes" : "No");
                    values.put("Available", "" + candy.i4f_Cnt());
                    values.put("Hard", candy.bff_Hrd() == 1 ? "Yes" : "No");
                    values.put("Shelled", candy.bff_Shl() == 1 ? "Yes" : "No");
                    values.put("Taffy", candy.bff_Taf() == 1 ? "Yes" : "No");
                    data.put("Candy Id", "" + candy.i4f_Id());
                    buttons = new String[]{"Search", "Next"};
                } else {
                    values.put("Error", "Not enough Candies selected.");
                }
            } catch (IllegalStateException ex) {
                values.put("Error", "Too many Candies selected.");
            }
        } else if (id == "Next") {
            redirect(new ToppingsSearchApp(id));
        }
    }
}


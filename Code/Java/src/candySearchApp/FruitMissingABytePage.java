// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


// ***************************************************
// ** This is "external code". You can't change it. **
// ***************************************************
package candySearchApp;

import java.util.Map;

// ***************************************************
// ** This is "external code". You can't change it. **
// ***************************************************
public abstract class FruitMissingABytePage {
    // ***************************************************
    // ** This is "external code". You can't change it. **
    // ***************************************************
    protected FruitMissingABytePage() {
        throw new IllegalStateException(
                // ***************************************************
                // ** This is "external code". You can't change it. **
                // ***************************************************
                "This code must be executed on our awesome, proprietary web server. Because it's the best server.");
    }

    // ***************************************************
    // ** This is "external code". You can't change it. **
    // ***************************************************
    protected abstract void defineFields(Map<String, String[]> options);

    protected abstract void setFields(Map<String, String> fieldValues);

    protected abstract void buttonPushed(String id);

    protected abstract Map<String, String> getProperties();

    protected abstract String[] defineButtons();

    protected void redirect(FruitMissingABytePage page) {
    }
}
// ***************************************************
// ** This is "external code". You can't change it. **
// ***************************************************


// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package candySearchApp;

import toppings.Topping;

import java.util.*;

public class ToppingsSearchApp extends FruitMissingABytePage {
    private final String id;
    private final Map<String, String> values = new HashMap<>();
    private String[] buttons = {"Back", "Search"};
    private Map<String, String> data;

    public ToppingsSearchApp(String id) {
        this.id = id;
        values.put("Candy Id", id);
    }

    @Override
    protected void buttonPushed(String id) {
        if ("Back".equals(id)) {
            redirect(new SearchForCandy());
        } else if ("Search".equals(id)) {
            Topping topping = null;
            try {
                if ("Yes".equals(data.get("Is Premium")) || "Either".equals(data.get("Is Premium"))) {
                    topping = new find.Find().itPremium(Integer.parseInt(this.id),
                            new find.Find.TypeOfTopping[]{new find.Find.CategoryTypeOfTopping(data.get("Kind")), new find.Find.FlavorTypeOfTopping(data.get("Flavor")),});
                }

                if (topping == null && ("No".equals(data.get("Is Premium")) || "Either".equals(data.get("Is Premium")))) {
                    topping = new find.Find().it(Integer.parseInt(this.id),
                            new find.Find.TypeOfTopping[]{new find.Find.CategoryTypeOfTopping(data.get("Kind")), new find.Find.FlavorTypeOfTopping(data.get("Flavor")),});
                }

                if (topping == null) {
                    values.put("Error", "Too few Toppings selected.");
                } else {
                    values.put("Name", topping.szf_Ttl());
                    buttons = new String[]{"Back", "Search", "Next"};
                }
            } catch (IllegalStateException ex) {
                values.put("Error", "Too many Toppings selected.");
            }
        } else {
            values.put("Error", "Button behavior not defined, yet: " + id);
        }
    }

    @Override
    protected String[] defineButtons() {
        return buttons;
    }

    @Override
    protected void defineFields(Map<String, String[]> options) {
        options.put("Kind", new String[]{"Syrup", "Sprinkles", "Powder", "Spread", "Flavor 'Enhancer'"});
        options.put("Flavor", new String[]{"Pure", "Chocolate", "Lemon", "Orange", "Salt", "Peanuts"});
        options.put("Is Premium", new String[]{"Yes", "No", "Either"});
    }

    @Override
    protected Map<String, String> getProperties() {
        return values;
    }

    @Override
    protected void setFields(Map<String, String> fieldValues) {
        data = fieldValues;
    }
}


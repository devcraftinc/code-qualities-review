﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace TangledWebsWeWeave.Find
{
  public class Find
  {
    public interface TypeOfCandy
    {}

    public interface TypeOfTopping
    {}

    public class CategoryTypeOfTopping : TypeOfTopping
    {
      public CategoryTypeOfTopping(string category)
      {
        Category = category;
      }

      public string Category { get;  }
    }

    public class FlavorTypeOfTopping : TypeOfTopping
    {
      public FlavorTypeOfTopping(string flavor)
      {
        Flavor = flavor;
      }

      public string Flavor { get; }
    }

    public class CompositeTypeOfCandy : TypeOfCandy
    {
      public CompositeTypeOfCandy(TypeOfCandy[] parts)
      {
        Parts = parts;
      }

      public TypeOfCandy[] Parts { get; }
    }

    public class HardTypeOfCandy : TypeOfCandy
    {}

    public class TaffyTypeOfCandy : TypeOfCandy
    {}

    public class FruitTypeOfCandy : TypeOfCandy
    {}

    public class ShelledTypeOfCandy : TypeOfCandy
    {}

    public class ChocolateTypeOfCandy : TypeOfCandy
    {}

    public class NougatTypeOfCandy : TypeOfCandy
    {}

    public Candy.Candy It(TypeOfCandy t)
    {
      var tableName = "e_candy";
      var where = "";

      var types = ((CompositeTypeOfCandy)t).Parts;
      var del = " WHERE ";
      foreach (var type in types)
      {
        if (type is HardTypeOfCandy)
        {
          where += del;
          where += "IsHard = 1";
          del = ", ";
        }
        else if (type is TaffyTypeOfCandy)
        {
          where += del;
          where += "IsTaffy=1";
          del = ", ";
        }
        else if (type is ShelledTypeOfCandy)
        {
          where = where + del + "IsShelled=1";
          del = ",";
        }
        else if (type is ChocolateTypeOfCandy)
        {
          where = where + del;
          where += "Type='Chocolate'";
          del = " ,";
        }
        else
        {
          where += del;
          where += "Type='Fruit'";
          del = ", ";
        }
      }

      var candies = RemoteServerDataWarehouse_v2.SelectAndParseCandy(tableName + where);

      if (candies.Length == 0)
        return null;

      if (candies.Length > 2)
        throw new InvalidOperationException();

      return candies[0];
    }

    public Candy.Candy[] Them(TypeOfCandy t)
    {
      var tableName = "e_candy";
      var where = "";

      var types = ((CompositeTypeOfCandy)t).Parts;
      var del = " WHERE ";
      foreach (var type in types)
      {
        if (type is HardTypeOfCandy)
        {
          where += del;
          where += "IsHard = 1";
          del = ", ";
        }
        else if (type is TaffyTypeOfCandy)
        {
          where += del;
          where += "IsTaffy=1";
          del = ", ";
        }
        else if (type is ShelledTypeOfCandy)
        {
          where = where + del + "IsShelled=1";
          del = ",";
        }
        else if (type is ChocolateTypeOfCandy)
        {
          where = where + del;
          where += "Type='Chocolate'";
          del = " ,";
        }
        else
        {
          where += del;
          where += "Type='Fruit'";
          del = ", ";
        }
      }

      return RemoteServerDataWarehouse_v2.SelectAndParseCandy(tableName + @where);
    }

    public Toppings.Topping It(int id, TypeOfTopping[] t)
    {
      var tableName = "e_toppings";
      var where = " WHERE cancompatid=" + id;

      var del = " ,";
      foreach (var typeOfTopping in t)
      {
        if (typeOfTopping is CategoryTypeOfTopping)
        {
          var category = (CategoryTypeOfTopping)typeOfTopping;
          where += del;
          where += "Category='" + category.Category + "'";
          del = ",";
        }

        if (typeOfTopping is FlavorTypeOfTopping)
        {
          var flavor = (FlavorTypeOfTopping)typeOfTopping;
          where += del;
          where += "Flavor='" + flavor.Flavor + "'";
          del = ",";
        }
      }

      var candies = RemoteServerDataWarehouse_v2.SelectAndParseTopping(tableName + where);

      if (candies.Length == 0)
        return null;

      if (candies.Length > 2)
        throw new InvalidOperationException();

      return candies[0];
    }

    public Toppings.Topping ItPremium(int id, TypeOfTopping[] t)
    {
      var tableName = "e_premtops";
      var where = " WHERE cancompatid=" + id;

      var del = " ,";
      foreach (var typeOfTopping in t)
      {
        if (typeOfTopping is CategoryTypeOfTopping)
        {
          var category = (CategoryTypeOfTopping)typeOfTopping;
          where += del;
          where += "PremiumCategory='" + category.Category + "'";
          del = ",";
        }

        if (typeOfTopping is FlavorTypeOfTopping)
        {
          var flavor = (FlavorTypeOfTopping)typeOfTopping;
          where += del;
          where += "PremiumFlavor='" + flavor.Flavor + "'";
          del = ",";
        }
      }

      var candies = RemoteServerDataWarehouse_v2.SelectAndParseTopping(tableName + where);

      if (candies.Length == 0)
        return null;

      if (candies.Length > 2)
        throw new InvalidOperationException();

      return candies[0];
    }
  }
}

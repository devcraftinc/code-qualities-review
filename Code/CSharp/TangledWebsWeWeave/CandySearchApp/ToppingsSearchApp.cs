﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using TangledWebsWeWeave.Toppings;

namespace TangledWebsWeWeave.CandySearchApp
{
  internal class ToppingsSearchApp : FruitMissingABytePage
  {
    private readonly string id;
    private readonly IDictionary<string, string> values = new Dictionary<string, string>();
    private string[] buttons = { "Back", "Search" };
    private IDictionary<string, string> data;

    public ToppingsSearchApp(string id)
    {
      this.id = id;
      values.Add("Candy Id", id);
    }

    protected override void ButtonPushed(string id)
    {
      if ("Back".Equals(id))
      {
        Redirect(new SearchForCandy());
      }
      else if ("Search".Equals(id))
      {
        Topping topping = null;
        try
        {
          if ("Yes".Equals(data["Is Premium"]) || "Either".Equals(data["Is Premium"]))
          {
            topping = new Find.Find().ItPremium(int.Parse(this.id),
              new Find.Find.TypeOfTopping[] { new Find.Find.CategoryTypeOfTopping(data["Kind"]), new Find.Find.FlavorTypeOfTopping(data["Flavor"]), });
          }

          if (topping == null && ("No".Equals(data["Is Premium"]) || "Either".Equals(data["Is Premium"])))
          {
            topping = new Find.Find().It(int.Parse(this.id),
              new Find.Find.TypeOfTopping[] { new Find.Find.CategoryTypeOfTopping(data["Kind"]), new Find.Find.FlavorTypeOfTopping(data["Flavor"]), });
          }

          if (topping == null)
          {
            values["Error"] = "Too few Toppings selected.";
          }
          else
          {
            values["Name"] = topping.szf_Ttl;
            buttons = new[] { "Back", "Search", "Next" };
          }
        }
        catch (InvalidOperationException)
        {
          values["Error"] = "Too many Toppings selected.";
        }
      }
      else
      {
        values["Error"] = "Button behavior not defined, yet: " + id;
      }
    }

    protected override string[] DefineButtons()
    {
      return buttons;
    }

    protected override void DefineFields(IDictionary<string, string[]> options)
    {
      options.Add("Kind", new[] { "Syrup", "Sprinkles", "Powder", "Spread", "Flavor 'Enhancer'" });
      options.Add("Flavor", new[] { "Pure", "Chocolate", "Lemon", "Orange", "Salt", "Peanuts" });
      options.Add("Is Premium", new[] { "Yes", "No", "Either" });
    }

    protected override IDictionary<string, string> GetProperties()
    {
      return values;
    }

    protected override void SetFields(IDictionary<string, string> fieldValues)
    {
      data = fieldValues;
    }
  }
}
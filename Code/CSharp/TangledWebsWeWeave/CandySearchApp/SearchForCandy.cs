﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;

namespace TangledWebsWeWeave.CandySearchApp
{
  class SearchForCandy : FruitMissingABytePage
  {
    private readonly IDictionary<string, string> values = new Dictionary<string, string>();
    private string[] buttons = { "Search" };
    private IDictionary<string, string> data;

    protected override IDictionary<string, string> GetProperties()
    {
      return values;
    }

    protected override string[] DefineButtons()
    {
      return buttons;
    }

    protected override void DefineFields(IDictionary<string, string[]> options)
    {
      options.Add("Is Hard", new[] { "Yes", "No" });
      options.Add("Type", new[] { "Nougat", "Chocolate", "Fruit" });
      options.Add("Is Taffy", new[] { "Yes", "No" });
      options.Add("Is Shelled", new[] { "Yes", "No" });
    }

    protected override void SetFields(IDictionary<string, string> fieldValues)
    {
      data = fieldValues;
    }

    protected override void ButtonPushed(string id)
    {
      if (id == "Search")
      {
        var qualities = new List<Find.Find.TypeOfCandy>();
        if ("Yes".Equals(data["Is Hard"]))
          qualities.Add(new Find.Find.HardTypeOfCandy());

        if ("Yes".Equals(data["Is Taffy"]))
          qualities.Add(new Find.Find.TaffyTypeOfCandy());

        if (!"No".Equals(data["Is Shelled"]))
          qualities.Add(new Find.Find.ShelledTypeOfCandy());

        if ("Nougat".Equals(data["Type"]))
          qualities.Add(new Find.Find.NougatTypeOfCandy());
        else if ("Chocolate".Equals(data["Type"]))
          qualities.Add(new Find.Find.ChocolateTypeOfCandy());
        else
          qualities.Add(new Find.Find.FruitTypeOfCandy());

        try
        {
          var candy = new Find.Find().It(new Find.Find.CompositeTypeOfCandy(qualities.ToArray()));
          if (candy != null)
          {
            values.Add("Name", candy.szf_Ttl);
            values.Add("In Stock", candy.i4f_Cnt > 0 ? "Yes" : "No");
            values.Add("Available", "" + candy.i4f_Cnt);
            values.Add("Hard", candy.bff_Hrd == 1 ? "Yes" : "No");
            values.Add("Shelled", candy.bff_Shl == 1 ? "Yes" : "No");
            values.Add("Taffy", candy.bff_Taf == 1 ? "Yes" : "No");
            data["Candy Id"] = "" + candy.i4f_Id;
            buttons = new[] { "Search", "Next" };
          }
          else
          {
            values.Add("Error", "Not enough Candies selected.");
          }
        }
        catch (InvalidOperationException)
        {
          values.Add("Error", "Too many Candies selected.");
        }
      }
      else if (id == "Next")
      {
        Redirect(new ToppingsSearchApp(id));
      }
    }
  }
}
